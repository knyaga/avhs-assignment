// Required packages
var express = require('express');
var bodyParser = require('body-parser');

// Initialize express server
var app = express();

// Connected server port
var PORT = process.env.PORT || 3000;

// Json Middleware: Only Json is accepted
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Load devices routes
require('./api/routes/devices')(app);

app.listen(PORT, function(){
  console.log('Server running on port ' + PORT);
});
