var avhsRequest = require('request').defaults({
  json: true,

});

module.exports = function(app) {

  // Device collection
  devices = [];

  // List of devices route
  app.get('/devices', function(req, res){

    url = 'https://dev-api.avhs.axis.com/site.php?api=JSON&a=retrieve&u=avhs-test&p=AVHStest1024!';
    avhsRequest({uri: url}, function (error, response, body) {
       if(!error && response.statusCode === 200) {
         devices.push(body);
         res.json(devices);
       }else {
         res.send(response.statusCode);
       }
    });

  });

  // Single device by id route
  app.get('/devices/:id', function(req, res){
    res.json("devices by id " + req.params.id);
  });

};
